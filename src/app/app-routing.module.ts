import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RentersPageComponent } from './renters-page/renters-page.component';
import { UserDetailPageComponent } from './user-detail-page/user-detail-page.component';
import { LeaseholdersPageComponent } from './leaseholders-page/leaseholders-page.component';
import { BuyersPageComponent } from './buyers-page/buyers-page.component';
import { SellersPageComponent } from './sellers-page/sellers-page.component';
import { BrokersPageComponent } from './brokers-page/brokers-page.component';

const routes: Routes = [
  {path: '', redirectTo: '/buyers', pathMatch: 'full'},
  { path: 'renters',
    component: RentersPageComponent
  },
  { path: 'leaseHolders',
    component: LeaseholdersPageComponent
  },
  { path: 'buyers',
    component: BuyersPageComponent
  },
  { path: 'sellers',
  component: SellersPageComponent
  },
  { path: 'brokers',
  component: BrokersPageComponent
  },
  { path: 'userDetail', 
    children: [
      { path: ':id', component: UserDetailPageComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
