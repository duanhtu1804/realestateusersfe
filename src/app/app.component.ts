import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  title = 'RealEstateFE';
  childComponent:any;

  public search(keyword: string) {
    this.childComponent.search(keyword)
  }

  public onActivate(componentRef){
    this.childComponent = componentRef;
  }
}
