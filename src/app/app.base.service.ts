import {
    Observable
} from 'rxjs';
import { Injectable } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Injectable()
export abstract class AppBaseService {

    constructor(private spinnerService: Ng4LoadingSpinnerService)
    {}

    public abstract getBuyers(): Observable<any>;
    public abstract getSellers(): Observable<any>;
    public abstract getBrokers(): Observable<any>;
    public abstract getRenters(): Observable<any>;
    public abstract getLeaseHolders(): Observable<any>;
    public abstract getUserDetail(facebookId: string):Observable<any>;

    public showSpinner()
    {
        this.spinnerService.show();
    }

    public hideSpinner()
    {
        this.spinnerService.hide();
    }
}