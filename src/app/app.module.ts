import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RentersPageComponent } from './renters-page/renters-page.component';
import { UserDetailPageComponent } from './user-detail-page/user-detail-page.component';
import { LeaseholdersPageComponent } from './leaseholders-page/leaseholders-page.component';
import { BuyersPageComponent } from './buyers-page/buyers-page.component';
import { SellersPageComponent } from './sellers-page/sellers-page.component';
import { BrokersPageComponent } from './brokers-page/brokers-page.component';
// import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AppBaseService } from './app.base.service';
import { AppMockService } from './app.mock.service';

@NgModule({
  declarations: [
    AppComponent,
    RentersPageComponent,
    UserDetailPageComponent,
    LeaseholdersPageComponent,
    BuyersPageComponent,
    SellersPageComponent,
    BrokersPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
    // { provide: AppBaseService, useClass: AppService },
    { provide: AppBaseService, useClass: AppMockService },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
