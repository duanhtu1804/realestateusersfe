import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import buyerConfig from '../buyerConfig.json';
import renterConfig from '../renterConfig.json';
import sellerConfig from '../sellerConfig.json';
import leaseHolderConfig from '../leaseHolderConfig.json';
import brokerConfig from '../brokerConfig.json';

@Component({
  selector: 'app-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.css']
})
export class UserDetailPageComponent implements OnInit {

  tabs = [
    {
      tab: 'Giao dịch',
      active : true
    },
    {
      tab: 'Bài đăng',
      active : false
    },
    {
      tab: 'Bình luận',
      active : false
    },
    {
      tab: 'Lượt like',
      active : false
    },
    {
      tab: 'Số lần được tag',
      active : false
    },
  ]
  facebookId : string;
  user:any;
  type = '';
  transactionTypeDefault = '';
  config:any;
  
  constructor(private route: ActivatedRoute,private appService: AppBaseService) { 
  }

  ngOnInit() {
     this.facebookId = this.route.snapshot.paramMap.get("id");
     this.type = this.route.snapshot.queryParamMap.get('type');
     this.appService.showSpinner();
     if(this.type === 'buyer') {
        this.transactionTypeDefault = 'Mua nhà';
        this.config = buyerConfig;
     }
     if(this.type === 'renter') {
        this.transactionTypeDefault = 'Thuê nhà';
        this.config = renterConfig;
     }
     if(this.type === 'seller') {
      this.transactionTypeDefault = 'Bán nhà';
      this.config = sellerConfig;
     }
     if(this.type === 'broker') {
      this.transactionTypeDefault = 'Bán nhà';
      this.config = brokerConfig;
     }
     if(this.type === 'leaseholder') {
      this.transactionTypeDefault = 'Cho thuê nhà';
      this.config = leaseHolderConfig;
     }
     this.appService.getUserDetail(this.facebookId).subscribe(data => {
       if(this.config[this.facebookId]) {
          data.name = this.config[this.facebookId].name;
          data.profilePicture = this.config[this.facebookId].profilePicture;
       }
       this.user = data;
       this.appService.hideSpinner();
     })
  }

  public changeTab(tabName: string)
  {
    const choosenTab = this.tabs.find(tab => tab.tab === tabName);
    choosenTab.active = true;
    this.tabs.forEach(tab => {
      if(tab.tab !== tabName)
        tab.active = false;
    })
  }

}
