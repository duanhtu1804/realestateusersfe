import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppBaseService } from './app.base.service';

@Injectable()
export class AppService  extends AppBaseService{
    private API_PATH = 'http://127.0.0.1:8000/'
    constructor(private http: HttpClient,spinnerService: Ng4LoadingSpinnerService)
    {
        super(spinnerService)
    }

    public getBuyers(): Observable<any>{
        return this.http.get(this.API_PATH + 'getBuyers');
    }

    public getSellers(): Observable<any>{
        return this.http.get(this.API_PATH + 'getSellers');
    }

    public getBrokers(): Observable<any>{
        return this.http.get(this.API_PATH + 'getBrokers');
    }

    public getRenters(): Observable<any>{
        return this.http.get(this.API_PATH + 'getRenters');
    }

    public getLeaseHolders(): Observable<any>{
        return this.http.get(this.API_PATH + 'getLeaseHolders');
    }

    public getUserDetail(facebookId: string):Observable<any> {
        return this.http.post<any>(this.API_PATH + 'getUserDetail',{facebookId});
    }
}