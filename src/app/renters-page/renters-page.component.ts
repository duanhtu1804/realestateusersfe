import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import config from '../renterConfig.json';

@Component({
  selector: 'app-renters-page',
  templateUrl: './renters-page.component.html',
  styleUrls: ['./renters-page.component.css']
})
export class RentersPageComponent implements OnInit {

  renters  = [];
  displayRenters = [];
  isFilter = false;
  constructor( private router: Router,private appService: AppBaseService) { 
  }

  ngOnInit() {
    this.appService.showSpinner();
    this.appService.getRenters().subscribe(renters => {
      renters.forEach(renter => {
        if(config[renter.facebookId]) {
          renter.name = config[renter.facebookId].name;
          renter.profilePicture = config[renter.facebookId].profilePicture;
        }
      });
      this.renters = renters;
      this.displayRenters = this.renters;
      this.appService.hideSpinner();
    })
  }

  public navigateUserDetail(facebookId: string) {
    // this.router.navigateByUrl('userDetail/' + id);
    this.router.navigate(['userDetail/' + facebookId], { queryParams: { type: 'renter' } });
  }

  public onChangeCluster(optionValue) {
    if(parseInt(optionValue) === 6)
    {
      this.displayRenters = this.renters;
    }
    if(parseInt(optionValue) === 0)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách ít tương tác');
    }
    if(parseInt(optionValue) === 1)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách hay inbox riêng');
    }
    if(parseInt(optionValue) === 2)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách hay hỏi xin hình phòng');
    }
    if(parseInt(optionValue) === 3)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách hay hỏi giả, hỏi thông tin phòng và inbox riêng');
    }
    if(parseInt(optionValue) === 4)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách tương tác bình luận nhiều');
    }
    if(parseInt(optionValue) === 5)
    {
      this.displayRenters = this.renters.filter(renter => renter.cluster === 'Khách không tương tác');
    }
  }

  public search(keyword:string) {
    if (keyword.length > 0) {
      this.displayRenters = this.renters.filter(renter => (renter.name && renter.name.includes(keyword)) || renter.facebookId.includes(keyword));
    } else {
      this.displayRenters = this.renters;
    }
  }
}
