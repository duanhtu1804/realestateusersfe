import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentersPageComponent } from './renters-page.component';

describe('RentersPageComponent', () => {
  let component: RentersPageComponent;
  let fixture: ComponentFixture<RentersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentersPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
