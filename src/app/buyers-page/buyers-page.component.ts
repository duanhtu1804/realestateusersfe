import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import config from '../buyerConfig.json';


@Component({
  selector: 'app-buyers-page',
  templateUrl: './buyers-page.component.html',
  styleUrls: ['./buyers-page.component.css']
})
export class BuyersPageComponent implements OnInit {

  buyers  = [];
  displayBuyers = [];
  config:any;
  constructor( private router: Router, private appService: AppBaseService) { 
    this.config = config;
  }

  ngOnInit() {
    this.appService.showSpinner();
    this.appService.getBuyers().subscribe(buyers => {
      buyers.forEach(buyer => {
        if(config[buyer.facebookId]) {
          buyer.name = config[buyer.facebookId].name;
          buyer.profilePicture = config[buyer.facebookId].profilePicture;
        }
      });
      this.buyers = buyers;
      this.displayBuyers = this.buyers;
      this.appService.hideSpinner();
    })
  }

  public navigateUserDetail(facebookId: string) {
    // this.router.navigateByUrl('userDetail/' + id);
    this.router.navigate(['userDetail/' + facebookId], { queryParams: { type: 'buyer' } });
  }

  public onChangeCluster(optionValue) {
    if(parseInt(optionValue) === 8)
    {
      this.displayBuyers = this.buyers;
    }
    if(parseInt(optionValue) === 0)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách ít tương tác');
    }
    if(parseInt(optionValue) === 1)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách hay inbox riêng');
    }
    if(parseInt(optionValue) === 2)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách hay hỏi giả, hỏi thông tin phòng và inbox riêng');
    }
    if(parseInt(optionValue) === 3)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách chủ yếu like các bài post bán nhà');
    }
    if(parseInt(optionValue) === 4)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách hay hỏi giá và inbox riêng');
    }
    if(parseInt(optionValue) === 5)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách hay inbox riêng và yêu cầu xem sổ hồng');
    }
    if(parseInt(optionValue) === 6)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách tương tác bình luận nhiều');
    }
    if(parseInt(optionValue) === 7)
    {
      this.displayBuyers = this.buyers.filter(renter => renter.cluster === 'Khách không tương tác');
    }
  }

  public search(keyword:string) {
    if (keyword.length > 0) {
      this.displayBuyers = this.buyers.filter(buyer => (buyer.name && buyer.name.includes(keyword)) || buyer.facebookId.includes(keyword));
    } else {
      this.displayBuyers = this.buyers;
    }
  }
}
