import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import config from '../brokerConfig.json';


@Component({
  selector: 'app-brokers-page',
  templateUrl: './brokers-page.component.html',
  styleUrls: ['./brokers-page.component.css']
})
export class BrokersPageComponent implements OnInit {

  brokers  = [];
  displayBrokers = [];
  config:any;
  constructor( private router: Router, private appService: AppBaseService) { 
    this.config = config;
  }

  ngOnInit() {
    this.appService.showSpinner();
    this.appService.getBrokers().subscribe(brokers => {
      brokers.forEach(broker => {
        if(config[broker.facebookId]) {
          broker.name = config[broker.facebookId].name;
          broker.profilePicture = config[broker.facebookId].profilePicture;
        }
      });
      this.brokers = brokers;
      this.displayBrokers = this.brokers;
      this.appService.hideSpinner();
    })
  }

  public navigateUserDetail(facebookId: string) {
    // this.router.navigateByUrl('userDetail/' + id);
    this.router.navigate(['userDetail/' + facebookId], { queryParams: { type: 'broker' } });
  }

  public search(keyword:string) {
    if (keyword.length > 0) {
      this.displayBrokers = this.brokers.filter(broker => (broker.name && broker.name.includes(keyword)) || broker.facebookId.includes(keyword));
    } else {
      this.displayBrokers = this.brokers;
    }
  }
}
