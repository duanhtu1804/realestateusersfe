import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import config from '../sellerConfig.json';

@Component({
  selector: 'app-sellers-page',
  templateUrl: './sellers-page.component.html',
  styleUrls: ['./sellers-page.component.css']
})
export class SellersPageComponent implements OnInit {

  sellers :any;
  displaySellers: any;
  constructor( private router: Router, private appService: AppBaseService) { 
  }

  ngOnInit() {
    this.appService.showSpinner();
    this.appService.getSellers().subscribe(sellers => {
      sellers.forEach(seller => {
        if(config[seller.facebookId]) {
          seller.name = config[seller.facebookId].name;
          seller.profilePicture = config[seller.facebookId].profilePicture;
        }
      });
      this.sellers = sellers;
      this.displaySellers = this.sellers;
      this.appService.hideSpinner();
    })
  }

  public navigateUserDetail(id: string) {
    this.router.navigate(['userDetail/' + id], { queryParams: { type: 'seller' } });
  }

  public search(keyword:string) {
    if (keyword.length > 0) {
      this.displaySellers = this.sellers.filter(seller => (seller.name && seller.name.includes(keyword)) || seller.facebookId.includes(keyword));
    } else {
      this.displaySellers = this.sellers;
    }
  }
}
