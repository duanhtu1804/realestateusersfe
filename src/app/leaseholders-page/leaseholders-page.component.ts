import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBaseService } from '../app.base.service';
import config from '../leaseHolderConfig.json';

@Component({
  selector: 'app-leaseholders-page',
  templateUrl: './leaseholders-page.component.html',
  styleUrls: ['./leaseholders-page.component.css']
})
export class LeaseholdersPageComponent implements OnInit {

  leaseHolders :any;
  displayHolders: any;
  constructor( private router: Router,private appService: AppBaseService) { 
  }

  ngOnInit() {
    this.appService.showSpinner();
    this.appService.getLeaseHolders().subscribe(holders => {
      holders.forEach(holder => {
        if(config[holder.facebookId]) {
          holder.name = config[holder.facebookId].name;
          holder.profilePicture = config[holder.facebookId].profilePicture;
        }
      });
      this.leaseHolders = holders;
      this.displayHolders = this.leaseHolders;
      this.appService.hideSpinner();
    })
  }

  public navigateUserDetail(id: string) {
    this.router.navigate(['userDetail/' + id], { queryParams: { type: 'leaseholder' } });
  }

  public search(keyword:string) {
    if (keyword.length > 0) {
      this.displayHolders = this.leaseHolders.filter(holder => (holder.name && holder.name.includes(keyword)) || holder.facebookId.includes(keyword));
    } else {
      this.displayHolders = this.leaseHolders;
    }
  }
}
