import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaseholdersPageComponent } from './leaseholders-page.component';

describe('LeaseholdersPageComponent', () => {
  let component: LeaseholdersPageComponent;
  let fixture: ComponentFixture<LeaseholdersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaseholdersPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaseholdersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
