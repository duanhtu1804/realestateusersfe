import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AppBaseService } from './app.base.service';
import buyersResult from './buyersResult.json';
import sellersResult from './sellersResult.json';
import brokersResult from './brokersResult.json';
import rentersResult from './rentersResult.json';
import leaseHoldersResult from './leaseHoldersResult.json';
import usersDetailResult from './usersDetailResult.json';

@Injectable()
export class AppMockService  extends AppBaseService{
    constructor(spinnerService: Ng4LoadingSpinnerService)
    {
        super(spinnerService)
    }

    public getBuyers(): Observable<any>{
        return of(buyersResult);
    }

    public getSellers(): Observable<any>{
        return of(sellersResult);
    }

    public getBrokers(): Observable<any>{
        return of(brokersResult);
    }

    public getRenters(): Observable<any>{
        return of(rentersResult);
    }

    public getLeaseHolders(): Observable<any>{
        return of(leaseHoldersResult);
    }

    public getUserDetail(facebookId: string):Observable<any> {
        return of(usersDetailResult.find(detail => detail.facebookId == facebookId));
    }
}